####Git clone
if [ -e my-redis ]; then
  echo "App exists"
else
  echo "App doesn't exist"
  sudo git clone https://wingyu@bitbucket.org/wingyu/my-redis.git
fi

#####cd into DIR
cd my-redis

############### Unbind port 80 which was attached to Apache
sudo kill `sudo lsof -t -i:6379`

##### Build/Run Container
sudo make build run
