####Git clone
if [ -e docker-myapp-2 ]; then
  echo "App exists"
else
  echo "App doesn't exist"
  sudo git clone https://wingyu@bitbucket.org/wingyu/docker-myapp-2.git
fi

#####cd into DIR
cd docker-myapp-2

#### Make sure got the latest version
sudo git fetch && sudo git reset --hard origin/master


############### Unbind port 80 which was attached to Apache
sudo kill `sudo lsof -t -i:80`

##### Build/Run Container
sudo make build run
