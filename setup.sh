#!/bin/bash

#######Update your apt sources
echo "Updating apt sources"
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates

sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

if [ -e /etc/apt/sources.list.d/docker.list ]; then
  echo "File exists"
else
  echo "File doesn't exist"

  sudo -s touch /etc/apt/sources.list.d/docker.list
  echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | sudo tee --append /etc/apt/sources.list.d/docker.list
fi

sudo apt-get update

sudo apt-get purge lxc-docker

sudo apt-cache policy docker-engine

############Prerequisites by Ubuntu Version¶
echo "Installing prerequisites..."
sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual -y

##########Install Docker
echo "Installing Docker...."
sudo apt-get install docker-engine -y
sudo apt-get upgrade docker-engine -y

sudo service docker start

sudo docker run hello-world

########Installing Git

sudo apt-get install git-all -y

######### Docker Compos
sudo apt-get install python-pip python-dev build-essential  -y
sudo pip install docker-compose
