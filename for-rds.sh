####Git clone
if [ -e docker-myapp ]; then
  echo "App exists"
else
  echo "App doesn't exist"
  sudo git clone https://wingyu@bitbucket.org/wingyu/docker-myapp.git
fi

#####cd into DIR
cd docker-myapp

############### Unbind port 80 which was attached to Apache
sudo kill `sudo lsof -t -i:80`

##### Build/Run Container
sudo make build run
