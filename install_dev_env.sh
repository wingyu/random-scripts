sudo apt-get update -y
sudo apt-get install autoconf automake bison build-essential curl git-core libapr1 libaprutil1 libc6-dev libltdl-dev libreadline6 libreadline6-dev libsqlite3-0 libsqlite3-dev libssl-dev libtool libxml2-dev libxslt-dev libxslt1-dev libyaml-dev ncurses-dev nodejs openssl sqlite3 zlib1g zlib1g-dev -y

sudo apt-get install git -y

gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3

curl -L get.rvm.io | bash -s stable

source ~/.profile

rvm install 2.3

rvm use 2.3
rvm --default use 2.3

gem install rails

git config --global user.name "Vincent Wong"
git config --global user.email "wingyu64@gmail.com"

#Docker
sudo apt-get -y  update
sudo apt-get install curl \
    linux-image-extra-$(uname -r) \
    linux-image-extra-virtual
	
sudo apt-get -y  install apt-transport-https \
                       ca-certificates

curl -fsSL https://yum.dockerproject.org/gpg | sudo apt-key add -

sudo add-apt-repository \
       "deb https://apt.dockerproject.org/repo/ \
       ubuntu-$(lsb_release -cs) \
       main"
	   
sudo apt-get -y  update

sudo apt-get -y install docker-engine

sudo docker run hello-world

