####Git clone
if [ -e my-proxy ]; then
  echo "App exists"
else
  echo "App doesn't exist"
  sudo git clone https://wingyu@bitbucket.org/wingyu/my-proxy.git
fi

#####cd into DIR
cd my-proxy

############### Unbind port 80 which was attached to Apache
sudo kill `sudo lsof -t -i:80`

##### Build/Run Container
sudo make build run
